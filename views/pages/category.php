<?php
if (empty($products)) :
    ?>

    <div>Пусто!!((</div>

<?php else :
    foreach ($products as $product) :
        ?>

        <div class="item">
            <img src="/images/<?= $product['image'] ?>" alt="<?= $product['title']; ?>" class="item-image">
            <div class="item-data">
                <div class="item-title">
                    <a href="/index.php?view=product&id=<?= $product['id'] ?>">
                        <?= $product['title']; ?>
                    </a>
                </div>
                <div class="item-price">Цена: <?= $product['price']; ?></div>
            </div>
        </div>

    <?php endforeach;
endif;
?>