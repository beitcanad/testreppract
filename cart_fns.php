<?php

function add_to_cart($id)
{
    if(isset($_SESSION['cart'][$id]))
    {
        $_SESSION['cart'][$id]++;
        return true;
    }
    else {
        $_SESSION['cart'][$id] = 1;
        return true;
    }
    return false;
}


function update_cart()
{
    foreach ($_SESSION['cart'] as $id => $quantity) {
        if($_POST[$id] == 0) {
            unset($_SESSION['cart'][$id]);
        }
        else {
            $_SESSION['cart'][$id] = $_POST[$id];
        }
    }
}


function total_items($cart)
{
    if(empty($cart)) return 0;

    $num_items = 0;
    if(is_array($cart)) {
        foreach ($cart as $id => $qty) {
            $num_items += $qty;
        }
    }
    return $num_items;
}


function total_price($cart)
{
    if(empty($cart)) return 0;

    $total_price = 0;
    if(is_array($cart)) {
        foreach ($cart as $id => $qty) {
            $query = "SELECT price FROM products WHERE id='$id'";
            $price_res = mysqli_query(db_connect(), $query);
            if($price_res) {
                $price = mysqli_fetch_row($price_res);
                $total_price += (int)$price[0] * (int)$qty;
            }
        }
    }
    return $total_price;
}