<?php

function db_connect()
{
    $host = 'amazon-mysql-identifier.chpu1zfmtpak.eu-central-1.rds.amazonaws.com';
    $user = 'root';
    $pass = '6CtR9ftR2muZhF';
    $db_name = 'amazon_mysql';
    $port = '3306';

    $connection = mysqli_connect($host, $user, $pass, $db_name, $port);
    mysqli_query($connection, "SET NAMES utf8");
    if(!$connection || !mysqli_select_db($connection, $db_name)) return false;
    return $connection;
}


function db_result_to_array($result = null)
{
    if(empty($result)) return false;

    $res_array = array();
    $count = 0;
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
        $res_array[$count] = $row;
        $count++;
    }
    return $res_array;
}


function get_products()
{
    $connect = db_connect();

    if(!$connect) return false;

    $query = "SELECT * FROM products ORDER BY id DESC";
    $result = mysqli_query($connect, $query);
    $result = db_result_to_array($result);

    return $result;
}


function get_product($id = null)
{
    $connect = db_connect();

    if(!$connect) return false;

    $query = "SELECT * FROM products WHERE id='$id'";
    $result = mysqli_query($connect, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    return $row;
}


function get_categories()
{
    $connect = db_connect();

    if(!$connect) return false;

    $query = "SELECT * FROM categories ORDER BY id DESC";
    $result = mysqli_query($connect, $query);
    $result = db_result_to_array($result);

    return $result;
}


function get_cat_products($cat_id = null)
{
    if(empty($cat_id)) return false;

    $connect = db_connect();
    if(!$connect) return false;

    $query = "SELECT * FROM products WHERE category='$cat_id' ORDER BY id DESC";
    $result = mysqli_query($connect, $query);
    $result = db_result_to_array($result);

    return $result;
}