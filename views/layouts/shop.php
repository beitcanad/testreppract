<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Internet Shop</title>
    <link rel="stylesheet" href="/styles/style.css">
</head>
<body>


<header id="header" class="header">
    <div id="wrapper" class="wrapper">
        <nav id="nav" class="nav">
            <ul>
                <li>
                    <a href="/">Главна</a>
                </li>
                
                <?php
                $categories = get_categories();
                if(!empty($categories)) :
                    foreach ($categories as $category) : ?>
                
                        <li>
                            <a href="/index.php?view=category&cat_id=<?= $category['cat_id'] ?>">
                                <?= $category['name']; ?>
                            </a>
                        </li>
                        
                <?php endforeach; 
                endif; ?>
                
                <li id="cart" class="cart">
                    <a href="/index.php?view=cart">Ваша корзина
                        (<?= $_SESSION['total_items']; ?>) - $<?= $_SESSION['total_price']; ?></a>
                </li>
            </ul>
        </nav>
    </div>
</header>

<section id="section" class="section">

    <div id="wrapper" class="wrapper">

        <?php include PATH . '/views/pages/' . $view . '.php'; ?>

    </div>

</section>

<footer id="footer" class="footer">
    <div id="wrapper" class="wrapper">

    </div>
</footer>

</body>
</html>