
<div class="item">
    <img src="/images/<?= $product['image'] ?>" alt="<?= $product['title']; ?>" class="item-image">
    <div class="item-data">
        <div class="item-title"><?= $product['title']; ?></div>
        <div class="item-description"><?= $product['description']; ?></div>
        <div class="item-price">Цена: <?= $product['price']; ?></div>
    </div>
    <div class="add">
        <a href="/index.php?view=add_to_cart&id=<?= $product['id']; ?>">Добавить в корзину</a>
    </div>
</div>
