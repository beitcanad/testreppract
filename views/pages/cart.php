<?php if ($_SESSION['cart'] != 0) : ?>

    <form action="index.php?view=update_cart" method="post" id="cartForm">

        <table id="myCart" align="center" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <th>Товар</th>
                <th>Цена</th>
                <th>Кол-во</th>
                <th>Всего</th>
            </tr>

            <?php foreach ($_SESSION['cart'] as $id => $quantity) :
                $product = get_product($id);
                ?>

                <tr>
                    <td align="center"><?= $product['title']; ?></td>
                    <td align="center"><?= $product['price']; ?> $</td>
                    <td align="center">
                        <input type="text" size="2" name="<?= $id; ?>" maxlength="2"
                               value="<?= $quantity; ?>">
                    </td>
                    <td align="center">$<?= $product['price']*$quantity; ?></td>
                </tr>

            <?php endforeach; ?>


        </table>
        <p class="total" align="center">Общая сумма заказа:
            <span>$ <?= $_SESSION['total_price']; ?></span>
        </p>
        <p align="center"><input type="submit" name="update" value="Обновить"></p>
    </form>

<?php else : ?>

    <p>Ваша корзина пуста</p>

<?php endif; ?>