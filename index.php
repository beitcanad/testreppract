<?php

define( "PATH", $_SERVER['DOCUMENT_ROOT']);

include PATH . "/db_connect.php";
include PATH . "/cart_fns.php";

session_start();
if(!isset($_SESSION['cart'])) {
    $_SESSION['cart'] = array();
    $_SESSION['total_items'] = 0;
    $_SESSION['total_price'] = '0.00';
}

$view = empty($_GET['view']) ? 'index' : $_GET['view'];

switch ($view)
{
    case 'index':
            $products = get_products();
        break;
    case 'cart': ;
        break;
    case 'category':
            if (!isset($_GET['cat_id'])) http_redirect('/');
            $cat_id = (string)$_GET['cat_id'];
            $products = get_cat_products($cat_id);
        break;
    case 'product':
            if (!isset($_GET['id'])) http_redirect('/');
            $id = (int)$_GET['id'];
            $product = get_product($id);
        break;
    case 'add_to_cart':
            if (!isset($_GET['id'])) http_redirect('/');
            $id = (int)$_GET['id'];
            $add_item = add_to_cart($id);
            $_SESSION['total_items'] = total_items($_SESSION['cart']);
            $_SESSION['total_price'] = total_price($_SESSION['cart']);
            header("Location: index.php?view=product&id=$id");
        break;
    case 'update_cart':
            update_cart();
            $_SESSION['total_items'] = total_items($_SESSION['cart']);
            $_SESSION['total_price'] = total_price($_SESSION['cart']);
            header("Location: index.php?view=cart");
        break;
}

include PATH . "/views/layouts/shop.php";
